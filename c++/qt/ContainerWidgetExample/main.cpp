#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>

class ContainerWidget : public QWidget
{
    Q_OBJECT

public:
    ContainerWidget(QWidget *parent = nullptr) : QWidget(parent)
    {
        QHBoxLayout* mainLayout = new QHBoxLayout();

        QLabel* label = new QLabel();
        label->setText("Hello, world!");
        mainLayout->addWidget(label);

        QLineEdit* edit = new QLineEdit();
        edit->setPlaceholderText("Write here...");
        mainLayout->addWidget(edit);

        QPushButton* button = new QPushButton();
        button->setText("Bye!");
        mainLayout->addWidget(button);

        this->setLayout(mainLayout);
    }
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ContainerWidget w;
    w.show();
    return a.exec();
}

#include "main.moc"
