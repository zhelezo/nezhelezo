#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyleSheet(
        "QLabel {"
            "border: 2px solid green;"
            "border-radius: 4px;"
            "padding: 2px;"

        "}"
        "QPushButton {"
            "background-color: #33cc66;"
            "border-style: outset;"
            "border-width: 2px;"
            "border-radius: 10px;"
            "border-color: beige;"
            "font: bold 14px;"
            "padding: 6px;"
        "}"
    );
    MainWindow w;
    w.show();
    return a.exec();
}
