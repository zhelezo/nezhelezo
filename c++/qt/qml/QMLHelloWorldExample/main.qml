import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 640
    height: 200
    visible: true
    title: qsTr("Hello World")
    color: "lightgray"

    Rectangle {
        anchors.centerIn: parent
        height: 100
        width: 300
        color: "#32cd32"
        radius: 12

        Text {
            anchors.centerIn: parent
            text: "Привет, мир!"
            font.pointSize: 16
        }
    }
}
