#include <QApplication>
#include <QWidget>
#include <QPainter>

class CustomWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CustomWidget(QWidget *parent = nullptr) : QWidget(parent) {}
protected:
    void paintEvent(QPaintEvent* e) override
    {
        QPainter painter(this);
        painter.setPen(QPen(Qt::black, 1, Qt::SolidLine, Qt::FlatCap));
        painter.setBrush(QBrush(Qt::lightGray, Qt::SolidPattern));
        painter.drawEllipse(this->rect());
        painter.drawText(this->rect(), Qt::AlignCenter, "Hello,\nWorld!");
        QWidget::paintEvent(e);
    }
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CustomWidget w;
    w.setFixedSize(100, 100);
    w.show();
    return a.exec();
}

#include "main.moc"
